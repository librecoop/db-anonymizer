# DATABASE ANONYMIZER

Python script to anonymize data from a database using [Faker](https://github.com/joke2k/faker).

## HOW TO USE

To use the anonymizer script follow these steps:

1. Install the python requirements with `pip install -r requirements.txt`
2. Install the required ODBC drivers ([pyodbc docs](https://github.com/mkleehammer/pyodbc/wiki/Drivers-and-Driver-Managers))
3. Setup the file *definitions.yml* with the desired database schema
4. Install de script locally with `pip install -e .`
5. Run the script with `dbanonymizer`

## FUTURE IMPROVEMENTS

- [X] Batch update instead of updating all rows at once for large tables
- [X] Definitions filename as parameter
- [X] Usage statistics
- [X] Add filter field to definitions.yml to allow use of _where_ clauses when obtaining records to anonymize

