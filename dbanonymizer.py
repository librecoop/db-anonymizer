import pyodbc
import yaml
import time
from faker import Faker
import click

BATCH_SIZE = 5000

@click.command()
@click.option('-d', '--definitions', default='./definitions.yml', type=click.Path(exists=True, readable=True), help='Path to the definitions yaml dile (default: ./definitions.yml)')
@click.option('-b', '--databases-file', type=click.Path(exists=True, readable=True), help='Path to the databases definition yaml file(overrides databases in definitions file)')
@click.option('-s', '--schemas-file', type=click.Path(exists=True, readable=True), help='Path to the schemas definition yaml file (overrides schemas in definitions file)')
def cli(definitions, databases_file, schemas_file):
    """
    This script anonymizes a database replacing the values specified in the definitions file with values generated with faker
    """

    fake = Faker(['es_ES'])
    with open(definitions, 'r') as file:
        definitions = yaml.safe_load(file)

    if (databases_file):
        with open(databases_file, 'r') as file:
            databases = yaml.safe_load(file)
    else:
        databases = definitions['databases']

    if (schemas_file):
        with open(schemas_file, 'r') as file:
            schemas = yaml.safe_load(file)
    else:
        schemas = definitions['schemas']

    for database in databases:
        start_time = time.perf_counter()
        click.echo('Opened Database: ' + click.style(f'{database["name"]} 💾', fg='blue'))
        # Specifying the ODBC driver, server name, database, etc. directly
        cnxn = pyodbc.connect(f"DRIVER={{{database['driver']}}};SERVER={database['server']};DATABASE={database['name']};UID={database['user']};PWD={database['password']}")
        cnxn.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
        cnxn.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
        cnxn.setencoding(encoding='utf-8')
        # Create a cursor from the connection
        read_cursor = cnxn.cursor()
        update_cursor = cnxn.cursor()
        for table in schemas[database['schema']]['tables']:
            fields = table['fields']
            field_names = map(lambda field: field['name'], fields)
            query = f"SELECT {table['id']}, {', '.join(field_names)} FROM {table['name']}"
            if 'filter' in table:
                query = query + f" WHERE {table['filter']}"
            read_cursor.execute(query)
            updated_rows = []
            number_of_updated_rows = 0
            fill_char = click.style('━', fg='green')
            empty_char = click.style('━', dim=True)
            with click.progressbar(read_cursor, length=read_cursor.rowcount, bar_template='%(label)s  %(bar)s  %(info)s',fill_char=fill_char, empty_char=empty_char, label='  ╰─ Anonymizing ' + click.style(read_cursor.rowcount, fg='blue') + ' rows of table ' + click.style(table['name'], fg='blue')) as rows:
                for row in rows:
                    values = ()
                    for field in fields:
                        if row.__getattribute__(field['name']):
                            values = values +(getattr(fake, field['type'], lambda : None)(),)
                        else:
                            values = values +(row.__getattribute__(field['name']),)
                    values = values +(row.__getattribute__(table['id']),)
                    updated_rows.append(values)
                    number_of_updated_rows += 1
                    if (len(updated_rows) >= BATCH_SIZE):
                        update_cursor.executemany(f"UPDATE {table['name']} SET {', '.join([f['name'] + ' = ?' for f in fields])} WHERE {table['id']} = ?", updated_rows)
                        cnxn.commit()
                        updated_rows.clear()

            update_cursor.executemany(f"UPDATE {table['name']} SET {', '.join([f['name'] + ' = ?' for f in fields])} WHERE {table['id']} = ?", updated_rows)
            cnxn.commit()
        end_time = time.perf_counter()
        click.echo(click.style('✔ ', fg='green') + click.style(database['name'], fg='blue') + ' anonymization completed in ' + click.style(end_time - start_time, fg='green') + ' senond(s)')