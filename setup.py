from setuptools import setup

setup(
    name='dbanonymizer',
    version='0.1.0',
    py_modules=['dbanonymizer'],
    install_requires=[
        'Click>=8.1.3',
        'pyodbc>=4.0.34',
        'pyyaml>=6.0',
        'Faker>=15.1.1'
    ],
    entry_points={
        'console_scripts': [
            'dbanonymizer=dbanonymizer:cli',
        ],
    },
)